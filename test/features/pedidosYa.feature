Feature: Feature name

   Feature Description

Scenario: Scenario name
    Given I go to www.pedidosya.com.uy
    And Set any street
    And search for "Pizza"
    And Click Search button
    And Confirm the street
    And Report total no. of results with no. of results in the current page
    And Select a random Filter
    And Verify the number of results is correct
    And Sort by: "Alfabéticamente" 
    And Verify the sort is correct
    And Report total no. of results with no. of results in the current page
    And Report the star rating of each of the results in the first result page
    And Go into the first result from the search results
    And Log all critical information of the selected restaurant

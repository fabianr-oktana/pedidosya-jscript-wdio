import { defineSupportCode } from 'cucumber';
import home from '../pageobjects/home.page'

defineSupportCode(function({ Given }) {
    Given(/^I go to www.pedidosya.com.uy$/, function() {
        home.navigate()
    });

    Given(/^Set any street$/, function() {
        home.inputStreet()
    });

    Given(/^search for "Pizza"$/, function() {
        home.inputFood()
    });

    Given(/^Click Search button$/, function() {
        home.clickSearch()
    });

    Given(/^Confirm the street$/, function() {
        home.confirmAdress()
    });
  });
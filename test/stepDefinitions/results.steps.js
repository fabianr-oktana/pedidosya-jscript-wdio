import { defineSupportCode } from 'cucumber';
import results from '../pageobjects/results.page'

defineSupportCode(function({ Given }) {
    Given(/^Report total no. of results with no. of results in the current page$/, function() {
        results.reportTotalResults()
        results.reportShowingResutls()

    });

    Given(/^Select a random Filter$/, function() {
        results.selectRandomFilter()
    });

    Given(/^Verify the number of results is correct$/, function() {
        expect(results.verifyFilter()).to.be.true;
    });

    Given(/^Sort by: "([^"]*)"$/, function(criteria) {
        results.sortBy(criteria)
    });

    Given(/^Verify the sort is correct$/, function() {
        expect(results.verifyOrder()).to.be.true;
    });

    Given(/^Report the star rating of each of the results in the first result page$/, function() {
        results.reportStars()
    });

    Given(/^Go into the first result from the search results$/, function() {
        results.goToFirstResult()
    });

    Given(/^Log all critical information of the selected restaurant$/, function() {
        results.restaurantInfo()
    });

  });
var FILTER_QTY_RESULTS
var ORDER_CRITERION
class Results {

    get found() {return browser.element('.found :only-child')}
    get rowsRest() {return browser.$$('.arrivalName')}
    get filterList() {return browser.$$('.js-filter-channel.old_channel')}
    get searchButton() {return browser.element('#search')}
    get confirmButton() {return browser.element('#confirm')}
    get orderDropDown() {return browser.element('.dropdown.withDrop')}
    get orderList() {return browser.elements('.f-dropdown.open')}
    get openRests() {return browser.$$('[data-status="OPEN"]')}
    get closedRests() {return browser.$$('[data-status="CLOSED"]')}
    get openSoonRests() {return browser.$$('[data-status^="SOON"]')}
    get rankStars() {return browser.$$('.ranking >i')}
    get distance() {return browser.elements('.distance')}
    get deliveryTime() {return browser.element('.deliveryTime')}
    get shippingAmount() {return browser.element('.shippingAmount')}
    get confirmClosedRest() {return browser.$$('#lnkConfirmPop')}
    get nameDetail() {return browser.getText('h1')}
    get hourChart() {return browser.$$('.dateshours >li')}
    get infoTab() {return browser.element('[data-link="tab-info"]')}
    get commentsTab() {return browser.element('[data-link="tab-comments"]')}
    get commentRow() {return browser.$$('#reviewList > li')}
    get commStars() {return browser.$$(this.commentRow[0].selector + '  .rating-points')}
    get commAuthor() {return browser.$$(this.commentRow[0].selector + '  [itemprop="author"]')}
    get commDate() {return browser.$$(this.commentRow[0].selector + '  [itemprop="datePublished"]')}
    get commText() {return browser.$$(this.commentRow[0].selector + '  [itemprop="description"]')}

    reportTotalResults() {
        this.found.waitForVisible(5000)
        console.log(this.found.getText() + " total results found")
    }

    reportShowingResutls() {
        console.log(this.rowsRest.length + " result showing") 
    }

    selectRandomFilter() {
        let random = Math.floor(Math.random() * this.filterList.length) 
        let filter = this.filterList[random]
        FILTER_QTY_RESULTS = filter.element('i').getText()
        filter.click()
    }

    verifyFilter () {
        console.log(this.found.getText() + " total results found for filter");
        console.log(FILTER_QTY_RESULTS +  " total results expected for filter");
        return this.found.getText() === FILTER_QTY_RESULTS
    }

    sortBy(criterion) {
        ORDER_CRITERION = criterion
        this.orderDropDown.click()
        browser.click('*=' + criterion)
    }

    verifyOrder() {
        switch (ORDER_CRITERION){
            case "Alfabéticamente":
                console.log("verifying opened restaurants")
                if (this.compareNames(this.openRests) === false){
                    return false
                }
                console.log("verifying soon opened restaurants")
                if (this.compareNames(this.openSoonRests) === false){
                    return false
                }
                console.log("verifying closed restaurantes")
                if (this.compareNames(this.closedRests) === false){
                    return false
                }
                return true
                break
            default:
                console.log("Criterion not established")
                return false

        }
    }

    compareNames(restStatus) {
        let restsQty = restStatus.length
        console.log(restsQty)
        if (restsQty > 1) {
            for(let i=0; i<restsQty -1; i++){
                let rest1 = restStatus[i].element(this.rowsRest[0].selector).getText()
                let rest2 = restStatus[i+1].element(this.rowsRest[0].selector).getText()
                if (rest2<rest1){
                    return false;
                }
            }
        }
        return true
    }

    reportStars() {
        let ratingQty = this.rankStars.length
        for (let i in this.rankStars){
            console.log("\nRestaurant " + i+1)
            console.log("Name: " + this.rowsRest[i].getText())
            console.log("Raiting: " + this.rankStars[i].getText())
        }
    }

    goToFirstResult(){
        this.rowsRest[0].click()
    }

    restaurantInfo() {
        this.logRestInfo()
        this.logComments()

    }

    logRestInfo() {
        let dist = this.distance.getText().replace(this.distance.element('small').getText(),"").replace("\n","")
        let time = this.deliveryTime.getText().replace(this.deliveryTime.element('small').getText(),"").replace("\n","")
        let cost = this.shippingAmount.getText().replace(this.shippingAmount.element('small').getText(),"").replace("\n","")
        let name = this.nameDetail
        console.log("Name of restaurant:     " + name)
        console.log("Distance:               " + dist)
        console.log("Time of delivery        " + time)
        console.log("Shipping ammount:       " + cost)
        console.log("")
        console.log("Open hours:")
        this.infoTab.click();
        this.hourChart[0].waitForVisible(3000)
        for (let i in this.hourChart){
            console.log(this.hourChart[i].getText())
        }
    }

    logComments() {
        this.commentsTab.click()
        let limit
        if(this.commentRow.length >= 5){
            limit = 4
        }else{
            limit=this.commentRow.length-1
        }
        console.log("Comments:")
        for (let i=0; i<=limit; i++){
            console.log("Comment " + parseInt (i+1))
            console.log("    Author:   " + this.commAuthor[i].getText())
            console.log("    Date:     " + this.commDate[i].getText())
            console.log("    Raiting:  " + this.commStars[i].getText())
            console.log("    Note:     " + this.commText[i].getText())
        }
    }

}
export default new Results()
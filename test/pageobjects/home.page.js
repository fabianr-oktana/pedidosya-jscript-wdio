class Home {

    get logo() {return browser.element('.logo')}
    get addressInput() {return browser.element('#address')}
    get optionalInput() {return browser.element('#optional')}
    get searchButton() {return browser.element('#search')}
    get confirmButton() {return browser.element('#confirm')}
    get map() {return browser.element('.gmnoprint > img')}
    
    navigate() {
        browser.url('https://www.pedidosya.com.uy/')
        this.logo.waitForVisible(5000)
    }

    inputStreet() {
        this.addressInput.setValue('Dr. Enrique Tarigo 1335')
    }

    inputFood() {
        this.optionalInput.setValue('Pizza')
    }

    clickSearch() {
        this.searchButton.click()
    }

    confirmAdress() {
        this.map.waitForVisible(5000)
        this.confirmButton.click()
    }
}
export default new Home()
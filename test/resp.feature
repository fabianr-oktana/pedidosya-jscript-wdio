Feature: Sign up a new user in qa.zola.com website

Background: Open site and click sign up
	Given User navigates to qa.zola.com
	And User clicks on Sign Up button

Scenario: Sign up a new user
	Given User inputs a valid and unregistered email address in email field
	And User inputs a valid password in password field
	When User clicks on Sign up for free button
	Then text "Welcome, this is the wedding of" must be shown on screen

Scenario: Sign up with already registered email
	Given User inputs a valid and already registered email address in email field
	And User inputs a valid password in password field
	When User clicks on Sign up for free button
	Then text "This email address is taken." must be shown bellow email field

Scenario: Sign up with empty email
	Given User leaves email field blank
	And User inputs a valid password in password field
	When User clicks on Sign up for free button
	Then text "Required" must be shown bellow email field

Scenario: Sign up with empty password
	Given User inputs a valid and unregistered email address in email field
	And User leaves password field blank
	When User clicks on Sign up for free button
	Then text "Required" must be shown bellow password field


Scenario Outline: Sign up with invalid email address
	Given User inputs <InvalidEmail> in the email field
	And User inputs a valid password in password field
	When User clicks on Sign up for free button
	Then text <InvalidEmailError> must be shown bellow email field
	Examples:
	| InvalidEmail | InvalidEmailError |
	| "test@ing" | "Invalid email address" |
	| "test.com" | "Invalid email address" |
	| "testing" | "Invalid email address" |
	| "test@.com" | "Invalid email address" |


Scenario: Sign up with password lesser than 8 characters
	Given User inputs a valid and unregistered email address in email field
	And User inputs a password of "6" characters
	When User clicks on Sign up for free button
	Then text "Password must be at least 8 characters long." must be shown bellow password field


Scenario: Sign up with password of exactly 8 characters
	Given: User inputs a valid and unregistered email address in email field
	And: User inputs a password of "8" characters
	When: User clicks on Sign up for free button
	Then: text "Welcome, this is the wedding of" must be shown on screen


Scenario: Sign up with password greater than 50 characters
	Given User inputs a valid and unregistered email address in email field
	And User inputs a password of "50" characters
	When User clicks on Sign up for free button
	Then text "Welcome, this is the wedding of" must be shown on screen
